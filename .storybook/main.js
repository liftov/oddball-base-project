const path = require('path')

module.exports = {
	stories: ['../src/**/*.stories.{tsx,mdx}'],
	addons: [
		'@storybook/addon-docs',
		'@storybook/addon-actions',
		'@storybook/addon-controls',
		// '@storybook/addon-a11y', will be used later on
		'@storybook/addon-viewport',
		'@storybook/addon-links',
		'@storybook/addon-essentials',
		'eslint-plugin-storybook',
	],
	staticDirs: ['../src/assets'],
	// https://gist.github.com/shilman/8856ea1786dcd247139b47b270912324
	core: {
		builder: 'webpack5',
	},
	webpackFinal: async (config) => {
		config.module.rules = [
			...config.module.rules,
			{
				test: /\.(js)$/,
				use: [
					{
						loader: require.resolve('babel-loader'),
						options: {
							presets: [
								// use @babel/preset-react for JSX and env (instead of staged presets)
								require.resolve('@babel/preset-react'),
								require.resolve('@babel/preset-env'),
							],
							plugins: [
								// use @babel/plugin-proposal-class-properties for class arrow functions
								require.resolve('@babel/plugin-proposal-class-properties'),
								// use babel-plugin-remove-graphql-queries to remove static queries from components when rendering in storybook
								require.resolve('babel-plugin-remove-graphql-queries'),
								// use babel-plugin-react-docgen to ensure PropTables still appear
								require.resolve('babel-plugin-react-docgen'),
							],
						},
					},
				],
				exclude: [/node_modules\/(?!(gatsby)\/)/],
			},
			{
				test: /\.s[ac]ss$/i,
				oneOf: [
					// module.scss files (e.g component styles.module.scss)
					// https://webpack.js.org/loaders/style-loader/#modules
					{
						test: /\.module\.s?css$/,
						use: [
							// Add exports of a module as style to DOM
							{
								loader: 'style-loader',
								options: {
									esModule: true,
								},
							},
							// Loads CSS file with resolved imports and returns CSS code
							{
								loader: 'css-loader',
								options: {
									esModule: true,
									modules: {
										namedExport: true,
									},
								},
							},
							// Loads and compiles a SASS/SCSS file
							{
								loader: 'sass-loader',
								// only if you are using additional global variable
								options: {
									additionalData: "@import 'src/styling/global.scss';",
									sassOptions: {
										includePaths: ['src/styling'],
									},
								},
							},
						],
					},
					// scss files that are not modules (e.g. custom.scss)
					{
						use: [
							// Add exports of a module as style to DOM
							'style-loader',
							// Loads CSS file with resolved imports and returns CSS code
							'css-loader',
							// Loads and compiles a SASS/SCSS file
							{
								loader: 'sass-loader',
								// only if you are using additional global variable
								options: {
									additionalData: "@import 'src/styling/global.scss';",
									sassOptions: {
										includePaths: ['src/styling'],
									},
								},
							},
						],
					},
				],
			},
			{
				test: /\.(png|jpe?g|gif)$/i,
				use: [
					{
						loader: 'file-loader',
					},
				],
			},
		]
		config.resolve = {
			...config.resolve,
			alias: {
				'@components':  path.resolve('src/components'),
			},
		}
		return config
	},
}