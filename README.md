# README #

This project serves as a starting point for Oddball websites. It consists of a basic [Gatsby](https://www.gatsbyjs.com/) project 

### What is this repository for? ###
 
* This project should be used for starting up any Oddball website and be maintained with all the most recent versions

### How do I get set up? ###

* ```npm install && npm start```

### Contribution guidelines ###

* Write [pull requests](https://bitbucket.org/liftov/oddball-base-project/pull-requests/) to include new features into this project
* Review pull requests [Bitbucket](https://bitbucket.org/liftov/oddball-base-project/src/master/) by any other developer