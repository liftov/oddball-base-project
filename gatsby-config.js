const sass = require('sass')

require('dotenv').config({
	path: `.env.${process.env.NODE_ENV}`,
})

module.exports = {
	siteMetadata: {
		siteUrl: 'https://www.yourdomain.tld',
		title: 'Oddball base project',
	},
	plugins: [
		`gatsby-plugin-react-helmet`,
		{
			resolve: 'gatsby-plugin-sass',
			options: {
				implementation: sass,
			},
		},
		{
			resolve: 'gatsby-plugin-eslint',
			options: {
				// Default settings that may be ommitted or customized
				stages: ['develop'],
				extensions: ['js', 'jsx', 'tsx', 'ts'],
				exclude: ['node_modules', '.cache', 'public'],
			},
		},
		{
			resolve: `gatsby-plugin-alias-imports`,
			options: {
				alias: {
					'@components': 'src/components',
					'@assets': 'src/assets',
					'@utils': 'src/utils',
				},
			},
		},
		{
			resolve: 'gatsby-plugin-google-tagmanager',
			options: {
				id: process.env.GOOGLE_TAG_MANAGER_ID,
			},
		},
	],
}
