import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import ActionButton from './ActionButton'

export default {
	title: 'Library/ActionButton',
	component: ActionButton,
	argTypes: {
		buttonType: {
			options: ['close'],
			control: { type: 'radio' },
		},
		size: {
			options: ['sm', 'md', 'lg'],
			control: { type: 'select' },
		},
		handleClick: { action: 'clicked' },
	},
} as ComponentMeta<typeof ActionButton>

export const Close: ComponentStory<typeof ActionButton> = (args) => (
	<ActionButton {...args} />
)

Close.args = {
	buttonType: 'close',
	disabled: false,
}
