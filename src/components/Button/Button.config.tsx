import React from 'react'

type propsConfig = {
	buttonType?: 'primary' | 'secondary' | string
	size?: 'sm' | 'md' | 'lg' | string
	disabled?: boolean
	isLoading?: boolean
	className?: string
	handleClick?: React.MouseEventHandler<HTMLButtonElement>
	children?: React.ReactNode
	label?: string
}

const defaultProps = {
	buttonType: 'primary',
	size: 'md',
	disabled: false,
	isLoading: false,
	className: '',
	handleClick: (): void => console.log('test'),
	children: undefined,
	label: 'Hello',
}

export { propsConfig, defaultProps }
