import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import Button from './Button'

export default {
	title: 'Library/Button',
	component: Button,
	argTypes: {
		buttonType: {
			options: ['primary', 'secondary'],
			control: { type: 'radio' },
		},
		size: {
			options: ['sm', 'md', 'lg'],
			control: { type: 'select' },
		},
		handleClick: { action: 'clicked' },
	},
} as ComponentMeta<typeof Button>

export const Primary: ComponentStory<typeof Button> = (args) => (
	<Button {...args} />
)
export const Secondary: ComponentStory<typeof Button> = (args) => (
	<Button {...args} />
)

export const Inactive: ComponentStory<typeof Button> = (args) => (
	<Button {...args} />
)

export const Loading: ComponentStory<typeof Button> = (args) => (
	<Button {...args} />
)

Primary.args = {
	buttonType: 'primary',
	disabled: false,
	isLoading: false,
}

Secondary.args = {
	buttonType: 'secondary',
	disabled: false,
	isLoading: false,
}

Inactive.args = {
	buttonType: 'primary',
	disabled: true,
	isLoading: false,
}

Loading.args = {
	buttonType: 'primary',
	disabled: true,
	isLoading: true,
}
