import React from 'react'

import { defaultProps, propsConfig } from './button.config'
import * as styles from './button.module.scss'

const Button: React.FC<propsConfig> = (props: propsConfig) => {
	return (
		<button
			type="button"
			className={`${styles.button} ${props.className} ${
				props.isLoading && styles.isLoading
			} ${styles[props.buttonType || 'primary']} ${styles[props.size || 'sm']}`}
			disabled={props.disabled || props.isLoading}
			onClick={props.handleClick || ((e) => e.preventDefault())}
		>
			{props.children}
			{props.label}
		</button>
	)
}

Button.defaultProps = defaultProps

export default Button
