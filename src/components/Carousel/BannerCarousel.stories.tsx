import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'
import { defaultProps } from './banner-carousel.config'

import BannerCarousel from './BannerCarousel'

export default {
	title: 'Library/BannerCarousel',
	component: BannerCarousel,
} as ComponentMeta<typeof BannerCarousel>

const Template: ComponentStory<typeof BannerCarousel> = (args) => <BannerCarousel {...args} />

export const Primary = Template.bind({})

Primary.args = defaultProps
