import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import Form from './Form'
import { defaultProps } from './form.config'

export default {
	title: 'Library/Form',
	component: Form,
} as ComponentMeta<typeof Form>

export const Primary: ComponentStory<typeof Form> = (args) => (
	<Form {...args} />
)

Primary.args = defaultProps
