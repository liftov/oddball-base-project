import React, { useState } from 'react'

import Input from '@components/Input/Input'
import Button from '@components/Button/Button'

import { validateField } from './form.utils'
import { propsConfig, defaultState } from './form.config'

import * as styles from './form.module.scss'

const Form: React.FC<propsConfig> = (props: propsConfig) => {
	const [formData, setFormData] = useState(defaultState)
	const [errorData, setErrorData] = useState(defaultState)
	const [loading, setLoading] = useState(false)

	const onSubmit = (e: React.FormEvent) => {
		e.preventDefault();
		let errors = { ...errorData }
		Object.entries(formData).forEach(([key, value]) => {
			errors = { ...errors, [key]: validateField(key, value) }
		})
		const hasErrors = Object.values(errors).some((value) => value.length)
		setErrorData(errors)

		if (!hasErrors) setLoading(true)
	}

	const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		setFormData({ ...formData, [e.target.id]: e.target.value })
		setErrorData({
			...errorData,
			[e.target.id]: validateField(e.target.id, e.target.value),
		})
	}

	return (
		<form className={`${styles.form} ${props.className}`} onSubmit={onSubmit}>
			<Input
				type="email"
				id="email"
				label="Email"
				value={formData.email}
				errorMessage={errorData.email}
				required
				placeholder="test@email.be"
				onChange={onChange}
			/>
			<Button isLoading={loading} handleClick={onSubmit} label="submit" />
		</form>
	)
}

export default Form
