type propsConfig = {
    className?: string
}

const defaultProps = {}

const defaultState = { email: '' }

export { propsConfig, defaultProps, defaultState }
