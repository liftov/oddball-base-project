import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import Input from './Input'
import { defaultProps } from './input.config'

export default {
	title: 'Library/Input',
	component: Input,
} as ComponentMeta<typeof Input>

export const Small: ComponentStory<typeof Input> = (args) => (
	<Input {...args} />
)

export const Medium: ComponentStory<typeof Input> = (args) => (
	<Input {...args} />
)

export const Large: ComponentStory<typeof Input> = (args) => (
	<Input {...args} />
)

Small.args = {...defaultProps, size: 'sm' }

Medium.args = {...defaultProps, size: 'md' }

Large.args = {...defaultProps, size: 'lg' }
