import React, { useState } from 'react'

import { defaultProps, propsConfig } from './input.config'

import * as styles from './input.module.scss'

const Input: React.FC<propsConfig> = (props: propsConfig) => {
	const [active, setActive] = useState(false)

	const onBlur = () => {
		setActive(false)
	}

	return (
		<label
			htmlFor={props.id}
			className={`
				${styles.label}
				${styles[props.size || 'sm']}
			`}
		>
			<p>{props.label ?? 'label'}{props.required && '*'}</p>
			<input
				className={`
					${styles.input} ${props.className}
					${active && styles.active}
					${props.errorMessage && styles.error}
					${styles[props.size || 'sm']}
				`}
				id={props.id}
				type={props.type}
				value={props.value}
				placeholder={props.placeholder}
				disabled={props.disabled}
				required={props.required}
				onClick={() => setActive(true)}
				onBlur={onBlur}
				onChange={(e: React.ChangeEvent<HTMLInputElement>) => props.onChange && props.onChange(e)}
			/>
			{props.errorMessage && (
				<p className={`${styles.error} ${styles[props.size || 'sm']}`}>
					{props.errorMessage}
				</p>
			)}
		</label>
	)
}

Input.defaultProps = defaultProps

export default Input
