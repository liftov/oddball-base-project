import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import LanguageSwitch from './LanguageSwitch'

export default {
	title: 'Library/Language switch',
	component: LanguageSwitch,
	argTypes: {
		size: {
			options: ['sm', 'md', 'lg'],
			control: { type: 'select' },
		},
		handleClick: { action: 'clicked' },
	},
} as ComponentMeta<typeof LanguageSwitch>

export const Primary: ComponentStory<typeof LanguageSwitch> = (args) => (
	<LanguageSwitch {...args} />
)

Primary.args = {
	size: 'md',
}
