import React from 'react'
import { getUniqueId } from '@utils/utils.service'

import { changeLanguage } from 'i18next'
import { useTranslation } from 'react-i18next'
import { defaultProps, propsConfig } from './languageSwitch.config'

import * as styles from './languageSwitch.module.scss'

const LanguageSwitch: React.FC<propsConfig> = (props: propsConfig) => {
	const { i18n } = useTranslation()

	return (
		<div className={`${styles.languageSwitch} ${props.className}`}>
			{Object.entries(i18n.services.resourceStore.data).map((language) => {
				return (
					<button
						onClick={() => changeLanguage(language[0])}
						type="button"
						key={getUniqueId()}
						className={`${styles.button} ${
							i18n.language === language[0] && styles.active
						}`}
					>
						{language[0]}
					</button>
				)
			})}
		</div>
	)
}

LanguageSwitch.defaultProps = defaultProps

export default LanguageSwitch
