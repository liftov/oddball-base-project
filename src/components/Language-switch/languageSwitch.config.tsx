type propsConfig = {
	size?: 'sm' | 'md' | 'lg' | string
	className?: string
}

const defaultProps = {
	size: 'md',
	className: '',
}

export { propsConfig, defaultProps }
