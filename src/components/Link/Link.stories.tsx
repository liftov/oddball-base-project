import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import Link from './Link'

export default {
	title: 'Library/Link',
	component: Link,
	argTypes: {
		linkType: {
			options: ['navigation', 'external'],
			control: { type: 'radio' },
		},
		size: {
			options: ['sm', 'md', 'lg'],
			control: { type: 'select' },
		},
		handleClick: { action: 'clicked' },
	},
} as ComponentMeta<typeof Link>

export const External: ComponentStory<typeof Link> = (args) => (
	<Link {...args} />
)
export const Navigation: ComponentStory<typeof Link> = (args) => (
	<Link {...args} />
)

External.args = {
	linkType: 'external',
	size: 'md',
	disabled: false,
	label: 'External link',
	blank: true,
}

Navigation.args = {
	linkType: 'navigation',
	size: 'md',
	disabled: false,
	label: 'Navigation link',
	blank: false,
}
