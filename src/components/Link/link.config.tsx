import React from 'react'

type propsConfig = {
	href: string
	linkType?: 'navigation' | 'external' | string
	size?: 'sm' | 'md' | 'lg' | string
	disabled?: boolean
	className?: string
	handleClick?: React.MouseEventHandler<HTMLButtonElement>
	children?: React.ReactNode
	label?: string
	blank?: boolean
}

const defaultProps = {
	href: '/test',
	linkType: 'external',
	disabled: false,
	size: 'md',
	className: '',
	children: undefined,
	label: 'Hello',
	blank: false,
}

export { propsConfig, defaultProps }
