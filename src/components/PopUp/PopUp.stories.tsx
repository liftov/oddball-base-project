import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import PopUp from './PopUp'

export default {
	title: 'Library/PopUp',
	component: PopUp,
	argTypes: {
		handleClick: { action: 'clicked' },
		closePopUp: { action: 'close' },
	},
} as ComponentMeta<typeof PopUp>

export const Primary: ComponentStory<typeof PopUp> = (args) => (
	<PopUp {...args} />
)

Primary.args = {
	open: true,
	backgroundBlur: true,
	allowClickOut: false,
	allowKeyPress: false,
}
