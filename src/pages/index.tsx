import React from 'react'

import * as styles from './index.module.scss'

const IndexPage = (): JSX.Element => {
	return (
		<main className={styles.index}>
			<h1>Oddball base project</h1>
		</main>
	)
}

export default IndexPage
